<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dua Ruang</title>
<!--
Cafe House Template
http://www.templatemo.com/tm-466-cafe-house
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Calibri' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.css"/>
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/templatemo-style.css"/>
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/font-awesome.min.css"/>
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.css">
  <link rel="shortcut icon" href="asset/img/favicon-16x16.png" type="image/x-icon" />

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
    <!-- Preloader -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Preloader -->
    <div class="tm-top-header">
      <div class="container">
        <div class="row">
          <div class="tm-top-header-inner">
            <div class="tm-logo-container">
              <img src="<?php echo base_url() ?>asset/img/favicon-32x32p.png" alt="Logo" class="tm-site-logo">
              <h1 class="tm-site-name tm-handwriting-font">Dua Ruang</h1>
            </div>
            <div class="mobile-menu-icon">
              <i class="fa fa-bars"></i>
            </div>
            <nav class="tm-nav">
              <ul>
                <li><a href="<?php echo site_url('Welcome') ?>">Home</a></li>
                <li><a href="<?php echo site_url('Pages/philosophy') ?>">Philosophy</a></li>
                <li><a href="<?php echo site_url('Pages/menu') ?>">Menu</a></li>
                <li><a href="<?php echo site_url('Pages/location') ?>">Location</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
