<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>DUARUANG</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/assets/css/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/plugins/loaders/pace.min.js"></script>
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/core/libraries/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/core/libraries/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script src="<?php echo base_url() ?>/asset_admin/assets/js/app.js"></script>
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/demo_pages/form_inputs.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php $this->view('admin/head'); ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->view('admin/menu'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Forms</span> - Edit User</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="#">User</a></li>
							<li class="active">Edit User</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Edit user</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">

                        
							<form class="form-horizontal" method="POST" action="<?php echo base_url() ?>user/update">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Nama</label>
                                    <input type="hidden" name="id_user" class="form-control" placeholder="Enter your name..." value="<?php echo $data_user->id_user ?>">
                                    <div class="col-lg-10">
                                        <input type="text" name="nama_user" class="form-control" placeholder="Enter your name..." value="<?php echo $data_user->nama_user ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-2">Username</label>
                                    <div class="col-lg-10">
                                        <input type="text" name="username" class="form-control" placeholder="Enter your username..." value="<?php echo $data_user->username ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-2">Password</label>
                                    <div class="col-lg-10">
                                        <input type="password" name="password" class="form-control" placeholder="Enter your password..." >
                                    </div>
                                </div>

								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
							</form>
						</div>
					</div>
					<!-- /form horizontal -->

					
					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2020. Andre Ardhiansyah
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
