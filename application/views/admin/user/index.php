<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>DUARUANG</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/assets/css/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url() ?>/asset_admin/assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/plugins/loaders/pace.min.js"></script>
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/core/libraries/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/core/libraries/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/plugins/forms/selects/select2.min.js"></script>

	<script src="<?php echo base_url() ?>/asset_admin/assets/js/app.js"></script>
	<script src="<?php echo base_url() ?>/asset_admin/global_assets/js/demo_pages/datatables_basic.js"></script>
	<!-- /theme JS files -->

</head>

<body>

    <?php $this->view('admin/head'); ?>


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
                <?php $this->view('admin/menu'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Tabel</span> - User</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_basic.html">Tabel User</a></li>
							<li class="active">User</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Tabel User</h5>
							<br>
							<a href="<?php echo base_url() ?>auth/user/add"><button type="button" class="btn btn-primary">Tambah User</button></a>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<!-- button add data -->
						</div>

						<table class="table datatable-basic">
							<thead>
								<tr>
                                    <th>No</th>
									<th>Name</th>
									<th>Username</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
                                <?php 
                                    $no = 1;
                                    foreach ($data_user as $result) {
                                ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td><?php echo $result->nama_user ?></td>
                                        <td><?php echo $result->username ?></td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="icon-menu9"></i>
                                                    </a>
    
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="<?php echo base_url() ?>user/edit/<?php echo $result->id_user ?>">Edit Data</a></li>
                                                        <li><a href="<?php echo base_url() ?>user/hapus/<?php echo $result->id_user ?>">Hapus Data</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
							</tbody>
						</table>
					</div>
					<!-- /basic datatable -->

					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2020. Andre Ardhiansyah
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
