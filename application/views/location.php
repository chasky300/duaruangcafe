
    <section class="tm-welcome-section">   
    </section>
    <div class="tm-main-section light-gray-bg2">
      <div class="container" id="main">
        <section class="tm-section row">
          
          <div class="col-lg-12 tm-section-header-container margin-bottom-30">
            <h2 class="tm-section-header gold-text tm-handwriting-font"><img src="<?php echo base_url() ?>asset/img/logo.png" alt="Logo" class="tm-site-logo"> Our Location</h2>
            <div class="tm-hr-container"><hr class="tm-hr"></div>
          </div>
          <h3 class="col-lg-12 margin-bottom-30" style=color:white;>Jl. Belimbing Indah Megah no.12 Pandanwangi, Blimbing, Malang City, East Java </h2>
         <!--- <form action="#" method="post" class="tm-contact-form">
                <img src="img/maps.png" width="1180" height="680">
          </form>
         -->
          <div class="col-lg-6 col-md-6">
            <div id="google-map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.5694053715793!2d112.65752611534263!3d-7.939957781284413!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd629719ac755d7%3A0xc70207bc236b20a8!2sDUA%20RUANG%20COFFEE!5e0!3m2!1sen!2sid!4v1588916022230!5m2!1sen!2sid" width="900" height="385" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
          </div> 
        </section>
      </div>
    </div> 

   
   <script>
    /* Google map
    ------------------------------------------------*/
    var map = '';
    var center;

    function initialize() {
      var mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(13.758468,100.567481),
        scrollwheel: false
      };
      
      map = new google.maps.Map(document.getElementById('google-map'),  mapOptions);

      google.maps.event.addDomListener(map, 'idle', function() {
        calculateCenter();
      });
      
      google.maps.event.addDomListener(window, 'resize', function() {
        map.setCenter(center);
      });
    }

    function calculateCenter() {
      center = map.getCenter();
    }

    function loadGoogleMap(){
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.5694053715793!2d112.65752611534263!3d-7.939957781284413!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd629719ac755d7%3A0xc70207bc236b20a8!2sDUA%20RUANG%20COFFEE!5e0!3m2!1sen!2sid!4v1588916022230!5m2!1sen!2sid' + 'callback=initialize';
      document.body.appendChild(script);
    }
    $(document).ready(function(){                
      loadGoogleMap();                
    });
    </script>
    </body>
    </html>
    