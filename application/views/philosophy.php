
    <section class="tm-welcome-section2">
      <div class="container tm-position-relative">
        <div class="row tm-welcome-content" style="margin-left: 150px;">

          <h2><img src="<?php echo base_url() ?>asset/img/logodr.png" width="100" height="100"></h2>
          <h2 class="gold-text tm-welcome-header-2">Dua Ruang</h2>
          <h2 class="white-text tm-handwriting-font tm-welcome-header"><img src="<?php echo base_url() ?>asset/img/header-line.png" alt="Line" class="tm-header-line">&nbsp;Coffee Philosophy&nbsp;&nbsp;<img src="<?php echo base_url() ?>asset/img/header-line.png" alt="Line" class="tm-header-line"></h2>
          <p style="text-align:center" class="white-text tm-welcome-description">Ceritaku bermula di Malang, Jawa Timur. Waktu kecil kedua orang tuaku hobinya adalah traveling dan dia juga sering mengahaku untuk ikut bersama mereka saaat raveling. Setiap kota yang kami kunjungi pasti ayahku selalu mencari kopi khas dari daerah tersebut. Karena ayahku adalah orang yang sangat suka kopi.

<br><br> Melalui Pengalama traveling bersama kedua orang tuaku. Aku memiliki berbagai macam pengalaman. Dari berbagai jenis kedai yang aku kunjungi, semua orang dari golongan hingga golongan bawah  meraka bisa menikmati enaknya kopi sambal berbicang dengan temannya dan juga keluarganya, dan melupakan kepenatan dan masalah untuk sementara.

<br><br>Saya berkeinginan untuk membuat kedai kopi dimana semua kalangan bisa menikmati kopi tersebut dan melupakan kepenatan atau masalah mereka. Saya juga berharap bisa membuat mereka merasa betah seperti rumah sendiri.

<br><br>Setelah saya lulus kuliah saya benar benar mendirikan Kafe Dua%Ruang dimana saya berkeinginan untuk menikmati kopi di tempat asal saya yaitu malang dan menikmati kopi bersama teman dan keluarga dengan segelas cangkir kopi yang luar biasa.

</p>

    </section>
