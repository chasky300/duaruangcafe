<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Superadmin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
    }

	public function index_login()
	{
		$this->load->view('admin/login');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('auth/admin');
    }
    
    public function dashboard() {
        if ($this->admin->logged_id()) {
            $this->load->view('admin/dashboard');
        } else {
            redirect('auth/admin');
        }
        
    }

    public function icon() {
        $this->load->view('admin/icons');
    }
	
}


?>