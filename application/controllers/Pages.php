<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Pages extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('menu');
    }

    public function menu()
    {
        $data = [
            'data_menu' => $this->menu->get_all()
        ];
        $this->load->view('header');
		$this->load->view('menu',$data);
		$this->load->view('footer');
    }

    public function philosophy()
    {
        $this->load->view('header');
        $this->load->view('philosophy');
        $this->load->view('footer');
    }

    public function location()
    {
        $this->load->view('header');
        $this->load->view('location');
        $this->load->view('footer');
    }

    public function coba()
    {
        $this->load->view('coba');
    }

}