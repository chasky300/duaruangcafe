<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
    }

	public function list_user() {
        $data = [
            'data_user' => $this->admin->get_all()
        ];

        $this->load->view('admin/user/index', $data);
    }

    
    public function page_tambah() {
        $data = [
            'title' => 'Tambah Data User'
        ];

        $this->load->view('admin/user/create', $data);
    }

    public function create() {
        $data = [
            'nama_user' => $this->input->post('nama_user'),
            'username' => $this->input->post('username'),
            'password' => MD5($this->input->post('password'))
        ];

        $this->admin->simpan($data);

        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success! data berhasil disimpan.</div>');

        redirect('auth/user');
    }

    public function edit($id_user)
    {
        $id_user = $this->uri->segment(3);

        $data = array(

            'title'     => 'Edit Data User',
            'data_user' => $this->admin->edit($id_user)

        );

        $this->load->view('admin/user/edit', $data);
    }

    public function update()
    {
        $id['id_user'] = $this->input->post("id_user");
        $data = [
            'nama_user' => $this->input->post('nama_user'),
            'username' => $this->input->post('username'),
            'password' => MD5($this->input->post('password'))
        ];

        $this->admin->update($data, $id);

        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success! data berhasil diupdate.</div>');

        //redirect
        redirect('auth/user');

    }

    public function hapus($id_user)
    {
        $id['id_user'] = $this->uri->segment(3);

        $this->admin->hapus($id);
        
        redirect('auth/user');

    }
	
}


?>