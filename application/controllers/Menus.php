<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('menu');
    }

	public function list_menu() {
        $data = [
            'data_menu' => $this->menu->get_all()
        ];

        $this->load->view('admin/menu/index', $data);
    }

    
    public function page_tambah() {
        $data = [
            'title' => 'Tambah Data Menu'
        ];

        $this->load->view('admin/menu/create', $data);
    }

    public function create() {
        $data = [
            'nama_menu' => $this->input->post('nama_menu'),
            'deskripsi_menu' => $this->input->post('deskripsi_menu'),
            'gambar_menu' => $this->input->post('gambar_menu'),
            'harga_menu' => $this->input->post('harga_menu'),
        ];

        $this->menu->simpan($data);

        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success! data berhasil disimpan.</div>');

        redirect('auth/menu');
    }

    public function edit($id_menu)
    {
        $id_menu = $this->uri->segment(3);

        $data = array(

            'title'     => 'Edit Data Menu',
            'data_menu' => $this->menu->edit($id_menu)

        );

        $this->load->view('admin/menu/edit', $data);
    }

    public function update()
    {
        $id['id_menu'] = $this->input->post("id_menu");
        $data = [
            'nama_menu' => $this->input->post('nama_menu'),
            'deskripsi_menu' => $this->input->post('deskripsi_menu'),
            'gambar_menu' => $this->input->post('gambar_menu'),
            'harga_menu' => $this->input->post('harga_menu'),
        ];

        $this->menu->update($data, $id);

        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success! data berhasil diupdate.</div>');

        //redirect
        redirect('auth/menu');

    }

    public function hapus($id_menu)
    {
        $id['id_menu'] = $this->uri->segment(3);

        $this->menu->hapus($id);
        
        redirect('auth/menu');

    }
	
}


?>