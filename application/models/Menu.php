<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Model
{
    //fungsi cek session
    function logged_id()
    {
        return $this->session->userdata('user_id');
    }

    //fungsi check login
    function check_login($table, $field1, $field2)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field1);
        $this->db->where($field2);
        $this->db->where($field3);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    public function get_all() {
        $query = $this->db->select('*')
                ->from('tbl_menu')
                ->order_by('id_menu', 'DESC')
                ->get();

        return $query->result();
    }

    public function simpan($data)
    {

        $query = $this->db->insert("tbl_menu", $data);

        if($query){
            return true;
        }else{
            return false;
        }

    }

    public function edit($id)
    {

        $query = $this->db->where("id_menu", $id)
                ->get("tbl_menu");

        if($query){
            return $query->row();
        }else{
            return false;
        }

    }

    public function update($data, $id)
    {

        $query = $this->db->update("tbl_menu", $data, $id);

        if($query){
            return true;
        }else{
            return false;
        }

    }

    public function hapus($id)
    {

        $query = $this->db->delete("tbl_menu", $id);

        if($query){
            return true;
        }else{
            return false;
        }

    }
}