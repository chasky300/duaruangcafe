<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_mod extends CI_Model{
    
    public function login($email, $password){
        $cek = $this->db->get_where('user', array('email'=>$email,'password'=>$password));
        if($cek->num_rows() == 1){
            $hasil = $cek->row_array();
            return $hasil;
        }
        else{
            return 0;
        }
    }
}